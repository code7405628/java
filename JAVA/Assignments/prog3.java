//Sum of odd and even number in the Array.

import java.io.*;
class sid{
	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

        System.out.println("Enter Array Size:");
	int size = Integer.parseInt(br.readLine());

        System.out.println("Enter Element:");
	int arr[] = new int[size];

	for(int i = 0;i<arr.length;i++){
		arr[i]=Integer.parseInt(br.readLine());
	}

	int sum1 = 0;
	int sum2 = 0;
	for(int i = 0;i<arr.length;i++){
		if(arr[i]%2==0){
			sum1 = sum1+arr[i];
		}else{
			sum2 = sum2+arr[i];
		}
	}
	
	System.out.println("Even count sum is = "+sum1);
	System.out.println("Odd count sum is = "+sum2);
	}
}
