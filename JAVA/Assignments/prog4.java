///find the index of the search element

import java.io.*;
class sid{
	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

        System.out.println("Enter Array Size:");
	int size = Integer.parseInt(br.readLine());

        System.out.println("Enter Element:");
	int arr[] = new int[size];

	for(int i = 0;i<arr.length;i++){
		arr[i]=Integer.parseInt(br.readLine());
	}
        System.out.println("Enter search element-");
	int search = Integer.parseInt(br.readLine());
	int flag = -1;
	int i = 0;
	for(;i<arr.length;i++){
		if(search == arr[i]){
			flag = i;
		}
	}
         System.out.println("Element is  found at Index is "+ flag);
	}
}			
