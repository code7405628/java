//find the maximum element element in the Array.

import java.io.*;
class sid{
	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

        System.out.println("Enter Array Size:");
	int size = Integer.parseInt(br.readLine());

        System.out.println("Enter Element:");
	int arr[] = new int[size];

	for(int i = 0;i<arr.length;i++){
		arr[i]=Integer.parseInt(br.readLine());
	}

	int max = arr[0];
	for(int i = 0;i<arr.length;i++){
		if(arr[i]>max){
			max = arr[i];
		}
	}
	System.out.println("Maximum element in the Array is "+ max);
	}
}
	
