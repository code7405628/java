import java.util.*;
class ArrayListDemo extends ArrayList{
	public static void main(String args[]){
	     ArrayList  al = new  ArrayList();

	    al.add(10) ;
	    al.add(20.5f) ;
	    al.add("Sid") ;
	    al.add(10) ;
	    al.add(20.5f) ;

	    System.out.println(al);

	 

	    System.out.println(al.size());
	    System.out.println(al.indexOf(20.5f));
	    System.out.println(al.lastIndexOf(20.5f));
	    System.out.println(al.contains("Sid"));
	    System.out.println(al.contains(30));
	    
	    System.out.println(al.get(3));
	    System.out.println(al.set(3,"Biencaps"));

	    System.out.println(al.remove(3));

	   // al.removeRange(3,5);
	    System.out.println(al);

	    Object  arr[] = al.toArray();
	    for(Object data:arr){

	            System.out.println(data+" ");
	    }

	    al.clear();
	    System.out.println(al);

	}
}

