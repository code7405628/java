import java.util.*;
class Movies{
        String movieName = null;
	double totcoll = 0.0;
        float imdbRating = 0.0f;

        Movies(String movieName,double totcoll, float imdbRating){
                this.movieName = movieName;
		this.totcoll =  totcoll;
                this.imdbRating = imdbRating;
        }
        public String toString(){
                return "{" + movieName + "," + totcoll + "," + imdbRating + "}";
        }
}
class SortByName implements Comparator{
        public int compare(Object obj1,Object obj2){
                return (((Movies)obj1).movieName.compareTo(((Movies)obj2).movieName));
        }
}

class SortBycoll implements Comparator{
        public int compare(Object obj1,Object obj2){
               return (int)((((Movies)obj1).totcoll) - ((((Movies)obj2).totcoll)));
        }
}

class SortByimdb implements Comparator{
        public int compare(Object obj1,Object obj2){
                return (int)((((Movies)obj1).imdbRating) - ((((Movies)obj2).imdbRating)));
        }
}
class UserListSort{
        public static void  main(String args[]){
         ArrayList al = new ArrayList();

         al.add(new Movies ("Bahubali",1700.00,9.8f));
         al.add(new Movies ("ved",70.00,7.5f));
         al.add(new Movies ("Sairat",100.00,2.1f));
         al.add(new Movies ("Gadar2",150.00,5.1f));
         

         System.out.println(al);          //print as it is--

         Collections.sort(al,new SortByName());

         System.out.println(al);

         Collections.sort(al,new SortBycoll());

         System.out.println(al);
        
         Collections.sort(al,new SortByimdb());
         
	 System.out.println(al); 
	}
}


