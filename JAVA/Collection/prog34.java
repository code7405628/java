import java.util.*;
class Project implements Comparable{
	String pName =  null;
	int cntTeam = 0;
	int duration = 0;

	Project(String pName,int cntTeam,int duration){
		this.pName = pName;
		this.cntTeam = cntTeam;
		this.duration = duration;
	}
	public String toString(){
		return "{" + pName + ":" + cntTeam + ":" + duration + "}";
               
	}
	public int compareTo(Object obj){
		return pName.compareTo(((Project)obj).pName);
	}
}
class Demo{
	public  static void main(String args[]){
		PriorityQueue pq = new PriorityQueue();

		pq.offer(new Project("java",5,80));
		pq.offer(new Project("c",7,110));
		pq.offer(new Project("python",11,140));

		System.out.println(pq);
	}
}
