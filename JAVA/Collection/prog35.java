import java.util.*;
class Project{
        String pName =  null;
        int cntTeam = 0;
        int duration = 0;

        Project(String pName,int cntTeam,int duration){
                this.pName = pName;
                this.cntTeam = cntTeam;
                this.duration = duration;
        }
        public String toString(){
                return "{" +pName+ ":" +duration+ "}";

        }
}
class SortByDuration implements Comparator{
	public int compare(Object obj1,Object obj2){
		return ((Project)obj1).duration-((Project)obj2).duration;
	}
}
class Demo{
        public  static void main(String args[]){
                PriorityQueue pq = new PriorityQueue(new SortByDuration());

            
                pq.offer(new Project("java",5,80));
                pq.offer(new Project("c",7,110));
                pq.offer(new Project("python",11,170));
             

                System.out.println(pq);
        }
}
