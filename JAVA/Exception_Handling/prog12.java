import java.util.*;
class DataoverflowException extends RuntimeException{
	DataoverflowException(String msg){
		super(msg);
	}
}
class DataunderflowException extends RuntimeException{
	DataunderflowException(String msg){
		super(msg);
	}
}
class ArrayDemo{
	public static void main(String args[]){
		int arr[] = new int[4];
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter integer value");
		System.out.println("Note: 0 < element < 100");

		for(int i = 0;i<arr.length;i++){
			int data = sc.nextInt();

			if(data < 0)
				throw new DataunderflowException("Data is less than zero");
			if(data>100)

				throw new DataoverflowException("Data is greater than  the hundred");

			arr[i] =  data;
		}

		for(int i = 0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
			
		}

	}
}
