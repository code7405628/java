class outer{
	void m1(){
		System.out.println("In m1-outer");
		class inner{
			void m1(){
		             System.out.println("In m1-inner");
			}
		}
		inner obj = new inner();
		obj.m1();
	}
	void m2(){

		System.out.println("In m2-outer");
	}
	public static void main(String args[]){
		outer obj = new outer();
		obj.m1();
		obj.m2();
	}
}

