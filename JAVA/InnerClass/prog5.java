class outer{
	void m1(){
		System.out.println("In m1-outer");
	}
	static class inner{
		void m1(){

		     System.out.println("In m1-inner");
		}
	}
}
class client{
	public static void main(String args[]){
		outer.inner obj = new outer.inner();
		obj.m1();

	}
}
