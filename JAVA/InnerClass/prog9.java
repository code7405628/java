class outer{
	class Inner{
		void fun2(){
			System.out.println("fun2-Inner");
		}
	}
	void fun1(){
			System.out.println("fun1-Outer");
	}

}
class client{
	public static void main(String args[]){
		outer obj = new outer();
		obj.fun1();

		outer.Inner obj1 = obj.new Inner();
		obj1.fun2();
	}
}

