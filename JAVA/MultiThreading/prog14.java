class MyThread extends Thread{
        MyThread(ThreadGroup tg,String str){
                super(tg,str);
        }
        public void run(){
                System.out.println(Thread.currentThread());
        }
}
class ThreadGroupDemo{
        public static void main(String args[]){
                ThreadGroup pthreadgp = new ThreadGroup("core2web");

                MyThread obj1 = new MyThread(pthreadgp,"c");
                MyThread obj2 = new MyThread(pthreadgp,"java");
                MyThread obj3 = new MyThread(pthreadgp,"python");

		obj1.start();
		obj2.start();
		obj3.start();

                ThreadGroup cthreadgp = new ThreadGroup(pthreadgp,"Incubator");

                MyThread obj4 = new MyThread(cthreadgp,"flutter");
                MyThread obj5 = new MyThread(cthreadgp,"nodejs");
                MyThread obj6 = new MyThread(cthreadgp,"reactjs");

		obj4.start();
		obj5.start();
		obj6.start();
	}
}

