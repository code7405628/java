class MyThread extends Thread{
	MyThread(ThreadGroup tg,String str){
		super(tg,str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
		try{
			Thread.sleep(5000);
		}
		catch(InterruptedException ie){
		        System.out.println(ie.toString());
		}
	}
}
class ThreadGroupDemo{
	public static void main(String args[])throws InterruptedException{
		ThreadGroup pThreadgp = new ThreadGroup("India");

		MyThread t1 = new MyThread(pThreadgp,"Maha");
		MyThread t2 = new MyThread(pThreadgp,"goa");

		t1.start();
		t2.start();

		ThreadGroup cThreadgp = new ThreadGroup(pThreadgp,"Pakistan");


		MyThread t3 = new MyThread(cThreadgp,"karachi");
		MyThread t4 = new MyThread(cThreadgp,"lahore");

		t3.start();
		t4.start();

		ThreadGroup cThreadgp2 = new ThreadGroup(pThreadgp,"Bangladesh");


		MyThread t5 = new MyThread(cThreadgp2,"Dhaka");
		MyThread t6 = new MyThread(cThreadgp2,"Mirpur");

		t5.start();
		t6.start();

		cThreadgp.interrupt();


		System.out.println(pThreadgp.activeCount());
		System.out.println(pThreadgp.activeGroupCount());
	}
}
