class MyThread implements Runnable{
	public void run(){

		System.out.println(Thread.currentThread());
		try{
			Thread.sleep(5000);
		}
		catch(InterruptedException ie){

		       System.out.println(ie.toString());
		}
	}
}
class ThreadGroupDemo{
	public static void main(String args[])throws InterruptedException{
		ThreadGroup pThreadgp = new ThreadGroup("India");

		MyThread obj1 = new MyThread();
		MyThread obj2 = new MyThread();

		Thread t1 = new Thread(pThreadgp,obj1,"Maha");
		Thread t2 = new Thread(pThreadgp,obj2,"Goa");

		t1.start();
		t2.start();

		System.out.println(pThreadgp.activeCount());
		System.out.println(pThreadgp.activeGroupCount());
	}
}




