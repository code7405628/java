abstract class Parent{
	void career(){
		System.out.println("Doctor");
	}
	abstract void marry();
}
class child extends Parent{
	void marry(){
		System.out.println("Alia Bhatt");
	}
}
class client{
	public static void main(String args[]){
		child obj = new child();
		obj.career();
		obj.marry();
	}
}
