class Parent{
                Parent(){

                        System.out.println(this);
                        System.out.println("In Parent Constructor");
                }
		void ParentProperty(){

                        System.out.println("Car,Gold,House");
		}

}
class Child extends Parent{
        Child(){
                  System.out.println(this);
		  System.out.println("In Child Constructor");
        }
}
class client{
        public static void main(String args[]){
                
               
                Child obj2 = new Child();
                System.out.println(obj2);
		obj2.ParentProperty();
		
        }
}

