class Parent{
	static{

		System.out.println("In Parent Const");
	}
}
class Child extends Parent{
	static{

		System.out.println("In Child Const");
	}
}
class client{
	public static void main(String args[]){
		Child obj = new Child();
	}
}
