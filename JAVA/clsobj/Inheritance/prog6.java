class Parent{
	static int x = 10;
	static{
		System.out.println("In Parent static Block");
	}
	static void access(){

		System.out.println(x);
	}
}
class Child extends Parent{
	static {

		System.out.println("In Child Static Block");
		System.out.println(x);
		access();
	}
}
class client{
	public static void main(String args[]){
		System.out.println("In main");
		Child obj = new Child();
	}
}
	

