class Parent{
	Parent(){

		System.out.println("Parent Constructor");
	}
	void fun(){
		
		System.out.println("In fun");
	}
}
class child extends Parent{
	child(){

		System.out.println("Child  Constructor");
	}
	void gun(){

		System.out.println("In gun");
	}
}
class client{
	public static void main(String args[]){
		child obj1 = new child();
		obj1.fun();
		obj1.gun();

		Parent obj2 = new Parent();
		obj2.fun();
		obj2.gun();
	}
}
