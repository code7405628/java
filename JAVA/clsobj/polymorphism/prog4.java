class Parent{
        Parent(){

                System.out.println("Parent Constructor");
        }
        void fun(){

                System.out.println("In Parent fun");
        }
}
class child extends Parent{
        child(){

                System.out.println("Child  Constructor");
        }
        void fun(){

                System.out.println("In Child fun");
        }
}
class client{
        public static void main(String args[]){
                Parent obj1 = new child();
                obj1.fun();
             
        }
}

