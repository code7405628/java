class player{
	private int jerNo =  0;
	private String name = null ;

	player(int jerNo,String name){
		this.jerNo = jerNo;
		this.name = name;
		System.out.println("In constructor");
	}
	void info(){
		System.out.println(jerNo + " = " + name);
	}

};
class client{
	public static void main(String args[]){
		player obj1 = new player(18,"virat");
		obj1.info();
		player obj2 = new player(7,"Dhoni");
		obj2.info();
		player obj3 = new player(45,"Rohit");
		obj3.info();
	}
}
